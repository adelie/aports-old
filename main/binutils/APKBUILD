# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=binutils
pkgver=2.29
pkgrel=2
pkgdesc="Tools necessary to build programs"
url="http://www.gnu.org/software/binutils/"
depends=""
makedepends_build="bison flex texinfo"
makedepends_host="zlib-dev"
makedepends="$makedepends_build $makedepends_host"
checkdepends="dejagnu"
arch="all"
license="GPL2 GPL3+ LGPL2 BSD"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
[ "${CARCH}" != "mips" ] && subpackages="$subpackages $pkgname-gold"
source="http://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.bz2
	binutils-ld-fix-static-linking.patch
	disable-gnu-mbind.patch
	disable-preinit-array-tests.patch
	fix-plt-main-bnd-test.patch
	mips-illegal-memcpy.patch
	remove-no-static-plt-test.patch
	remove-pr2404-tests.patch
	remove-pr19553c-test.patch
	gold-mips.patch
	"
builddir="$srcdir/$pkgname-$pkgver"

if [ "$CHOST" != "$CTARGET" ]; then
	pkgname="$pkgname-$CTARGET_ARCH"
	subpackages=""
	options="!check"
	sonameprefix="$pkgname:"
fi

# secfixes:
#   2.28-r1:
#   - CVE-2017-7614

build() {
	local _sysroot=/
	local _cross_configure="--enable-install-libiberty"
	local _arch_configure=""

	if [ "$CHOST" != "$CTARGET" ]; then
		_sysroot="$CBUILDROOT"
		_cross_configure="--disable-install-libiberty"
	fi

	if [ "$CTARGET_ARCH" = "x86_64" ]; then
		_arch_configure="--enable-targets=x86_64-pep"
	fi

	case "$CTARGET_ARCH" in
	mips*)  _hash_style_configure="--enable-default-hash-style=sysv" ;;
	*)      _hash_style_configure="--enable-default-hash-style=gnu"	;;
	esac

	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--with-build-sysroot="$CBUILDROOT" \
		--with-sysroot=$_sysroot \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-multilib \
		--enable-shared \
		--enable-ld=default \
		--enable-64-bit-bfd \
		--enable-plugins \
		--enable-relro \
		--enable-gold \
		--enable-deterministic-archives \
		$_cross_configure \
		$_arch_configure \
		$_hash_style_configure \
		--disable-werror \
		--disable-nls \
		--with-system-zlib \
		|| return 1
	make || return 1
}

package() {
	cd "$builddir"
	make install DESTDIR="$pkgdir" || return 1
	if [ -d "$pkgdir"/usr/lib64 ]; then
		mv "$pkgdir"/usr/lib64/* "$pkgdir"/usr/lib/
		rmdir "$pkgdir"/usr/lib64
	fi
	if [ "$CHOST" != "$CTARGET" ]; then
		# creating cross tools: remove any files that would conflict
		# with the native tools, or other cross tools
		rm -r "$pkgdir"/usr/share
		rm -f "$pkgdir"/usr/lib/libiberty.a
	fi
}

check() {
	cd "$builddir"
	# We can't run the gold test suite, because it cannot be used
	# on a system with default PIE/PIC.
	make -C binutils check
	make -C gas check
	make -C ld check
}

libs() {
	pkgdesc="Runtime libraries from binutils - libbfd and libopcodes"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*.so "$subpkgdir"/usr/lib/ || return 1
}

gold() {
	pkgdesc="GNU binutils - gold linker"

	if [ -e "$pkgdir"/usr/bin/ld.gold ]; then
		mkdir -p "$subpkgdir"/usr/bin
		mv "$pkgdir"/usr/bin/ld.gold "$subpkgdir"/usr/bin
	fi
	mkdir -p "$subpkgdir"/usr/$CTARGET/bin
	mv "$pkgdir"/usr/$CTARGET/bin/ld.gold "$subpkgdir"/usr/$CTARGET/bin/ld.gold
}

sha512sums="8148587d7e4f14ebcbcb3f984b116deaae5d4008228628acde14bc242a64a4b53faf1f6077a2c4ca4750e2f254b698ba506bd657f79e1202e87e7029b0069337  binutils-2.29.tar.bz2
ecee33b0e435aa704af1c334e560f201638ff79e199aa11ed78a72f7c9b46f85fbb227af5748e735fd681d1965fcc42ac81b0c8824e540430ce0c706c81e8b49  binutils-ld-fix-static-linking.patch
d378fdf1964f8f2bd0b1e62827ac5884bdf943aa435ec89c29fc84bb045d406b733fffaff8fdd8bd1cba8ddea7701c4cf6ccf3ed76a8a3df9c72b447737575a6  disable-gnu-mbind.patch
3537752e63cef0b5ef136d003ff7e814ba66b12624d817430112d0f291a792e8960fa69a78036f526af835441b3ee483d6a53d55c7b3dd8ee96f0399682dbcbe  disable-preinit-array-tests.patch
01149e3b772c92783048cc65a4fe6d63770c9f8351ff087901679ba886ccad0bdaeec83033be4537651570de0b042f2cb8e2fad0bcab70afae773053dfda11be  fix-plt-main-bnd-test.patch
06422157349abf02e79a5ef8bd9f51100e7e996aab65250d518e0cf0d7ac8ed922d3bf1603c4f5b4fd8fb179266b7b4c41db32dcb241d60a7f1c21d1df0c36dd  mips-illegal-memcpy.patch
b40f9a3841a7af8fc12e8a4044cd672df5614bfda8461b0ca45efa57a42c3bc8490e491ea490c6c05d319a52d69993c4fca33a0aeb044090e7b7f4e4e30c6517  remove-no-static-plt-test.patch
32ab4215669c728648179c124632467573a3d4675e79f0f0d221c22eb2ec1ca5488b79910bd09142f90a1e0d0b81d99ca4846297f4f9561f158db63745facb66  remove-pr2404-tests.patch
39ef9c76dd5db6b15f11ffa8061f7ca844fb79c3fb9879c3b1466eef332a28b833597c87003ab9f260b1b85023fae264659088aee27cad7e5aa77b2d58b9a3f6  remove-pr19553c-test.patch
f55cf2e0bf82f97583a1abe10710e4013ecf7d64f1da2ef8659a44a06d0dd8beaf58dab98a183488ea137f03e32d62efc878d95f018f836f8cec870bc448556f  gold-mips.patch"
