# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgbase=ConsoleKit2
pkgname=consolekit2
pkgver=1.2.0
pkgrel=6
pkgdesc="A framework for defining and tracking users, login sessions, and seats"
provides="consolekit=$pkgver"
replaces=consolekit
arch=all
url="https://consolekit2.github.io/ConsoleKit2"
license=GPL2
depends="polkit eudev"
makedepends="git automake autoconf gettext-dev glib-dev zlib-dev libxslt-dev
	polkit-dev eudev-dev libdrm-dev libnih-dev libtool linux-pam-dev
	xorg-server-dev acl-dev xmlto docbook-xml libevdev-dev"
checkdepends="libxml2-utils"
source="$pkgname-$pkgver.tar.gz::https://github.com/${pkgname}/${pkgname}/archive/${pkgver}.tar.gz
	consolekit2.initd
	ac_disable_static.patch
	0001-busybox-reboot-and-poweroff-support.patch
	add-listseats.patch
	pam-foreground-compat.ck"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-openrc"
builddir="$srcdir"/$pkgbase-$pkgver

prepare() {
	default_prepare

	cd "$builddir"

	NOCONFIGURE=1 ./autogen.sh
}

build() {
	cd "$builddir"
	XMLTO_FLAGS='--skip-validation' ./configure \
		--prefix=/usr --sysconfdir=/etc --localstatedir=/var \
		--disable-static --enable-pam-module --enable-udev-acl \
		--enable-tests --enable-docbook-docs --enable-polkit
	sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	install -m 755 "$srcdir"/pam-foreground-compat.ck \
		"$pkgdir"/usr/lib/ConsoleKit/run-session.d/
	install -D -m755 "$srcdir"/consolekit2.initd \
		"$pkgdir"/etc/init.d/consolekit
}
sha512sums="30f2ceec14044669f40676e9b17513874350e2b70f0a918be934f7e64309c8595dbec4ac0937044c98dda51eb97c99443dc9d1de33f08365d72da8600296ad78  consolekit2-1.2.0.tar.gz
8c16c452707475bdd4a50d3ade367d52ad92a6560be48b4e21e5b5eadef6e56c39d3d03d3a64f9b45a59eca50179cf5aa9c11978904d5d101db7498fb9bc0339  consolekit2.initd
0f628fd1589b1790ad9adcb2278de504b75cc6b4ec7284a1cbda44ebd34b9966014989f47f343cb936d8503acc4eeec43ddff07cb11f55388e47256b8420e2e8  ac_disable_static.patch
ec0c88e640afac0561c84131d63fa8c9e2e29611b789ae5c163cd11465b22017602d88dc853866624f57bce2ee466ab63af075d083a9ba6e87327ad8d0a0769f  0001-busybox-reboot-and-poweroff-support.patch
c2adfad3f7f6d5f880e0b7e7ed99f62bd7f2bd510492bba5634f0b2391bacd43cb3246a072400392c508d42acdba114cb920f5d498b0c4339d86cf19f691b6fa  add-listseats.patch
3b114fbbe74cfba0bfd4dad0eb1b85d08b4979a998980c1cbcd7f44b8a16b0ceca224680d4f4a1644cd24698f8817e5e8bdfcdc4ead87a122d0e323142f47910  pam-foreground-compat.ck"
