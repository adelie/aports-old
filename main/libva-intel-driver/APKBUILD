# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libva-intel-driver
pkgver=1.7.3
pkgrel=1
pkgdesc="VA-API implementation for Intel GMA and HD Graphics"
url="http://freedesktop.org/wiki/Software/vaapi"
arch="x86 x86_64"
license="MIT"
depends=""
makedepends="libdrm-dev libva-dev autoconf automake libtool"
install=""
subpackages=""
install_if="libva xf86-video-intel"
source="http://www.freedesktop.org/software/vaapi/releases/libva-intel-driver/libva-intel-driver-$pkgver.tar.bz2"

prepare() {
	cd "$builddir"
	default_prepare
	# we need to regen the configure script which will unconditionally
	# depend on wayland scanner otherwise
	libtoolize --force && aclocal -I m4 && autoconf \
		&& automake --add-missing
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-x11 \
		--disable-wayland \
		--disable-static \
		--enable-shared
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="90567b94e3373287b1b5fe776e291dfd33577bbe5e4ec1bee359c88544c9c17bbae2aac511ff3d704d7b99b19fbc908c9a38ebbd3162e3625c86c416142390e6  libva-intel-driver-1.7.3.tar.bz2"
