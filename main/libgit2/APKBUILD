# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Pierre-Gilas MILLON <pgmillon@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libgit2
pkgver=0.26.0
pkgrel=0
pkgdesc="A linkable library for Git"
url="https://libgit2.github.com/"
arch="all"
license="GPL-2.0"
depends=""
depends_dev="curl-dev libssh2-dev"
makedepends="$depends_dev python3 cmake zlib-dev openssl-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz
	build-both-static-dynamic.patch
	"
builddir="$srcdir/$pkgname-$pkgver"

# secfixes:
#   0.25.1-r0:
#   - CVE-2016-10128
#   - CVE-2016-10129
#   - CVE-2016-10130
#   0.24.3-r0:
#   - CVE-2016-8568
#   - CVE-2016-8569

build() {
	cd "$builddir"
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS"
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b6e51f2216c7c23f352572b780ea1325a25a517396709f036bb573295c2bd02aa505ba616846ac7e07863e99e640e7d47fefc5727478a257b283da99060ee47c  libgit2-0.26.0.tar.gz
39534d10f38f394446f93df810233464807fca3b0e903ee40067971ecbe1d78102bbe04283435032f757f970e6846ecf279eb727ab137c01e84427bd16913ee6  build-both-static-dynamic.patch"
