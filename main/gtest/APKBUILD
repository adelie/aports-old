# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=gtest
pkgver=1.8.0
pkgrel=2
pkgdesc="Google Test - C++ testing utility based on the xUnit framework (like JUnit)"
url="https://github.com/google/googletest"
arch="all"
options="!check"  # No test suite.
license="BSD"
depends="libgcc bash"
depends_dev="python3 cmake"
makedepends="$depends_dev"
install=""
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/google/googletest/archive/release-$pkgver.tar.gz"

builddir="$srcdir"/googletest-release-${pkgver}

build() {
	cd "$builddir"
	rm -rf build
	mkdir build
	cd build

	cmake -DBUILD_SHARED_LIBS=ON \
	      -DCMAKE_SKIP_RPATH=ON ../
	make
}

package() {
	cd "$builddir"
	for dir in usr/lib usr/include/gtest/internal/custom usr/share/licenses/gtest\
			usr/src/gtest/cmake usr/src/gtest/src; do
		install -d -m 0755 "$pkgdir"/"$dir"
	done
	install -m 0644 build/googlemock/gtest/libgtest*.so "$pkgdir"/usr/lib

	install -m 0644 googletest/include/gtest/*.h "$pkgdir"/usr/include/gtest
	install -m 0644 googletest/include/gtest/internal/*.h \
		"$pkgdir"/usr/include/gtest/internal/
	install -m 0644 googletest/include/gtest/internal/custom/*.h \
		"$pkgdir"/usr/include/gtest/internal/custom/
	install -m 0644 googletest/LICENSE "$pkgdir"/usr/share/licenses/$pkgname/
	install -m 0644 googletest/CMakeLists.txt "$pkgdir"/usr/src/gtest/
	install -m 0644 googletest/cmake/* "$pkgdir"/usr/src/gtest/cmake/
}

md5sums="16877098823401d1bf2ed7891d7dce36  release-1.8.0.tar.gz"
sha256sums="58a6f4277ca2bc8565222b3bbd58a177609e9c488e8a72649359ba51450db7d8  release-1.8.0.tar.gz"
sha512sums="1dbece324473e53a83a60601b02c92c089f5d314761351974e097b2cf4d24af4296f9eb8653b6b03b1e363d9c5f793897acae1f0c7ac40149216035c4d395d9d  release-1.8.0.tar.gz"
