# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=glib
pkgver=2.54.2
pkgrel=0
pkgdesc="Common C routines used by Gtk+ and other libs"
url="https://developer.gnome.org/glib/"
arch="all"
license="GPL"
depends=
triggers="$pkgname.trigger=/usr/share/glib-2.0/schemas:/usr/lib/gio/modules"
depends_dev="perl python3 attr-dev gettext-dev zlib-dev bzip2-dev libffi-dev
	util-linux-dev"
makedepends="$depends_dev pcre-dev xmlto"
checkdepends="tzdata"
options="!checkroot"
source="https://download.gnome.org/sources/$pkgname/${pkgver%.*}/$pkgname-$pkgver.tar.xz
	0001-gquark-fix-initialization-with-c-constructors.patch
	broken-gio-tests.patch
	fix-spawn.patch
	musl-no-locale.patch
	ridiculous-strerror-nonconformance.patch
	thread-test-fix.patch
	"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-static $pkgname-dev $pkgname-lang $pkgname-bash-completion:bashcomp:noarch"
builddir="$srcdir"/$pkgname-$pkgver

prepare() {
	cd "$builddir"
	default_prepare

	# workaround packaging issue. gtk-doc.make timestamp was newer than
	# Makefile.am, which triggers automake re-run
	touch -r docs/reference/glib/Makefile.am gtk-doc.make
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--disable-gtk-doc \
		--disable-compile-warnings \
		--disable-selinux \
		--with-pcre=system \
		--with-pic \
		--enable-static
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/usr/lib/charset.alias
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/usr/share
		find "$pkgdir"/usr/bin ! -name "glib-compile-schemas" -a \( \
		-name "gdbus-codegen" -o \
		-name "gobject-query" -o \
		-name "gresource" -o \
		-name "gtester*" -o \
		-name "glib-*" \) \
		-exec mv {} "$subpkgdir"/usr/bin \;
	mv "$pkgdir"/usr/share/gdb "$pkgdir"/usr/share/glib-2.0 \
		"$subpkgdir"/usr/share
}

static() {
	pkgdesc="glib static libraries"
	depends="gettext-static"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
}

bashcomp() {
	pkgdesc="Bash completion for $pkgname"
	depends=
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/bash-completion "$subpkgdir"/usr/share
	[ "$(ls -A "$pkgdir"/usr/share)" ] || rmdir "$pkgdir"/usr/share
}

sha512sums="09ee6fa3a6f3f15af229bd789bef536e3570f36d1e4ce624a57e97c4040577f6baccd6ab5746257863ccf7173b558cfa753951d562a278f854e52604104ba7ee  glib-2.54.2.tar.xz
32e5aca9a315fb985fafa0b4355e4498c1f877fc1f0b58ad4ac261fb9fbced9f026c7756a5f2af7d61ce756b55c8cd02811bb08df397040e93510056f073756b  0001-gquark-fix-initialization-with-c-constructors.patch
9bf99de4672765704759098c883cfc4d2747cf10d9d568ae97134806a089e4bebae57886bae45dcc53694e0190248abe6ae52cc38dc742cd754d352406ac0680  broken-gio-tests.patch
0f0a98784aeed92f33cd9239d2f668bdc6c09b84ed020825ae88f6aacf6a922152dc3e1384c40d9f30f54c5ab78fe17e0ee5c42b268b297b595d2a6cde5b8998  fix-spawn.patch
bdbed2241c7acf5f7e416f73a2c4883b4a72569d280519b9521552de7dd761c3d246baca157271bccab0b45921237584a4453c9a9f8f24f5cb56490d5351baa1  musl-no-locale.patch
56c10a0f64cbd8ce584d428f818e7e678fdeb40a32df792843208ddfa3135d362cc2077bc9fe3bfebe13ee6af0ecf6403a593ad727e0a92276074a17a9c7029c  ridiculous-strerror-nonconformance.patch
0cebf9cbf87a92c3160054eb30189a827847f5820a8b90f4842b4ad5ab5cc343ba06e5f55214864bd0f0d5a21e55ec5e7f35c66207e77b1496142b7ee0c75567  thread-test-fix.patch"
