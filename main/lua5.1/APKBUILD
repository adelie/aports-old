# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=lua5.1
_pkgname=lua
_majorver=${pkgname#lua}
pkgver="$_majorver.5"
_pkgver=$pkgver
pkgrel=4
pkgdesc="A powerful light-weight programming language designed for extending applications."
url="http://www.lua.org/"
arch="all"
license="MIT"
depends=""
depends_dev="$pkgname"
makedepends="libtool autoconf automake"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="http://www.lua.org/ftp/lua-$_pkgver.tar.gz
	lua-5.1-make.patch
	lua-5.1-module_paths.patch
	lua-5.1-readline.patch
	"
builddir="$srcdir/lua-$_pkgver"

prepare() {
	default_prepare

	cd "$builddir"

	# we want packages to find our things
	sed -i -e 's:/usr/local:/usr:' etc/lua.pc

	# correct lua versioning
	sed -i -e 's/\(LIB_VERSION = \).*/\16:4:1/' src/Makefile

	# we use libtool
	cat > configure.ac <<-EOF
		top_buildir=.

		AC_INIT(src/luaconf.h)
		AC_PROG_LIBTOOL
		AC_OUTPUT()
	EOF

	libtoolize --force --install && aclocal && autoconf
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr

	cd "$builddir"/src
	make CFLAGS=" -DLUA_USE_LINUX $CFLAGS" \
		RPATH="/usr/lib" \
		LIB_LIBS="-lpthread -lm -ldl" \
		V=$pkgver \
		alpine_all
}

check() {
	cd "$builddir"
	make test
}

package() {
	cd "$builddir"

	make V=$pkgver \
		INSTALL_TOP="$pkgdir/usr" \
		INSTALL_INC="$pkgdir/usr/include" \
		INSTALL_LIB="$pkgdir/usr/lib" \
		alpine_install

	local i; for i in "$pkgdir"/usr/bin/*; do
		mv $i ${i}$_majorver
	done

	install -D -m 644 etc/lua.pc "$pkgdir"/usr/lib/pkgconfig/lua$_majorver.pc
	install -D -m 644 doc/lua.1 "$pkgdir"/usr/share/man/man1/lua$_majorver.1
	install -D -m 644 doc/luac.1 "$pkgdir"/usr/share/man/man1/luac$_majorver.1
}

dev() {
	default_dev || return 1
	replaces="lua-dev"
}

libs() {
	pkgdesc="Lua dynamic library runtime"
	replaces="lua lua-libs"

	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr/
}

sha512sums="0142fefcbd13afcd9b201403592aa60620011cc8e8559d4d2db2f92739d18186860989f48caa45830ff4f99bfc7483287fd3ff3a16d4dec928e2767ce4d542a9  lua-5.1.5.tar.gz
ec5945f9f73d87fceaaa2418f1dc5c0f1e2ab1392e3a110e9ca737bfd122a951543899cd9b6170771374c35de8dd106f7b51ba9885eae281241c79b47ee58370  lua-5.1-make.patch
123b115389d01d383af8698d2ed66c75bb7c3e2d33de57b8539ae56a9478930a0a72fa5657e78cc1c5b6bef00d07ec3a51e19ec0e419647e84eb158fb5c4795a  lua-5.1-module_paths.patch
a99154258419dc2e582804eae97517687e5b8b5f731dc91722193d1ff470b8522af6ff4e7142c9258afe4734cd52e6987d3c2108ac52b1487a262f1e89f2f332  lua-5.1-readline.patch"
