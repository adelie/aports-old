# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=vim
pkgver=8.0.1240
pkgrel=0
pkgdesc="advanced text editor"
url="http://www.vim.org"
arch="all"
license="custom"
depends=""
options="!check"  # requires controlling TTY, and fails with musl locales
makedepends="acl-dev ncurses-dev perl-dev python3-dev"
subpackages="$pkgname-doc ${pkgname}diff::noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz
	vimrc
	"
builddir="$srcdir/$pkgname-$pkgver"

# secfixes:
#   8.0.0329-r0:
#     - CVE-2017-5953
#   8.0.0056-r0:
#     - CVE-2016-1248

prepare() {
	cd "$builddir"
	# Read vimrc from /etc/vim
	echo '#define SYS_VIMRC_FILE "/etc/vim/vimrc"' >> src/feature.h
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-perlinterp \
		--enable-python3interp=dynamic \
		--without-x \
		--enable-acl \
		--enable-nls \
		--enable-multibyte \
		--enable-gui=no \
		--with-compiledby="Adélie Linux"
	make
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir/" install

	install -Dm644 runtime/doc/uganda.txt \
		"$pkgdir/usr/share/licenses/$pkgname/LICENSE"
	install -Dm644 "$srcdir"/vimrc "$pkgdir"/etc/vim/vimrc
}

vimdiff() {
	pkgdesc="view file diffs in vim"
	depends="diffutils"

	install -d "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/vimdiff "$subpkgdir"/usr/bin
}

sha512sums="12f213b9fa61294359e5376605d4d7cd4a329fcfd1b3bcdbf3c9e4c4baf6bf0cc013a7670a173d84e0ef6a210705ddba8fc39a8222744b3779d6493d92c99d29  vim-8.0.1240.tar.gz
95d4ad2e86c4490814fa6047c9d2d57a8acffb6c286a81483faac44d0648874dc9669fee7d29ce98612f5ab8afe68480c29e4128a7aa7f99465dd8de298145c3  vimrc"
