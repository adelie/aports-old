# Contributor: <kalonji@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=zsh
pkgver=5.4.1
pkgrel=0
pkgdesc="A very advanced and programmable command interpreter (shell)"
url="http://www.zsh.org/"
arch="all"
license="BSD"
makedepends="ncurses-dev pcre-dev"
install="zsh.post-install zsh.post-upgrade zsh.pre-deinstall"
source="http://www.zsh.org/pub/$pkgname-$pkgver.tar.gz
	zprofile
	"
subpackages="$pkgname-doc $pkgname-calendar::noarch $pkgname-vcs::noarch $pkgname-zftp"
builddir="$srcdir/$pkgname-$pkgver"

_libdir="usr/lib/zsh/$pkgver"
_sharedir="usr/share/zsh/$pkgver"

# Move some bigger completion files to subpackages.
_comps="android-tools:Unix/_adb
	bzr:Unix/_bzr
	cvs:Unix/_cvs
	gcc:Unix/_gcc
	git:Unix/_git
	graphicsmagick:Unix/_graphicsmagick
	imagemagick:Unix/_imagemagick
	lynx:Unix/_lynx
	mercurial:Unix/_hg
	rsync:Unix/_rsync
	subversion:Unix/_subversion
	tmux:Unix/_tmux
	zfs:Unix/_zfs*:Unix/_zpool"
for _i in $_comps; do
	subpackages="$subpackages ${_i%%:*}-zsh-completion:_completion:noarch"
done

prepare() {
	cd "$builddir"

	default_prepare
	update_config_sub

	# Remove completions for other systems.
	cd Completion
	rm -Rf AIX BSD Cygwin Darwin Debian Mandriva Redhat Solaris openSUSE

	# Remove completions for programs that are not available on Alpine
	# (just to decrease size of the package).
	cd Unix/Command
	rm -f _aap _apm _baz _bittorrent _bpython _ccal _cdcd _chkconfig _clay \
		_cowsay _cplay _cssh _darcs _devtodo _dict _dsh _elfdump _elm \
		_enscript _finger _flasher _fsh _gnupod _guilt _initctl _lzop \
		_mencal _module _monotone _moosic _mysqldiff _nkf \
		_pack _perforce _pine _pkgadd _pkginfo _pkgrm _prcs \
		_quilt _raggle _rcs _rlogin _rubber _sablotron _sisu _socket \
		_stgit _surfraw _tardy _tin _tla _topgit _totd _twidge \
		_unace _unison _units _uzbl _vcsh _vux _wiggle _xmms2 _yodl
	cd ../../Linux/Command
	rm -f _mondo _tpb _tpconfig _uml _vserver
	cd ../../X/Command
	rm -f _acroread _dcop _gnome-gv _gqview _gv _kfmclient _matlab \
		_nautilus _netscape _qiv _vnc _xfig _xloadimage \
		_xournal _xv _xwit
}

build() {
	cd "$builddir"

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--bindir=/bin \
		--enable-etcdir=/etc/zsh \
		--enable-multibyte \
		--enable-pcre \
		--enable-function-subdirs \
		--enable-zsh-secure-free \
		--sysconfdir=/etc \
		--with-tcsetpgrp \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make
}

check() {
	cd "$builddir"
	# Does not work with musl due to UTF-8
	rm "$builddir"/Test/A03quoting.ztst
	# Does not work with musl due to locale
	rm "$builddir"/Test/B03print.ztst
	# Not guaranteed to work portably (requires atime)
	rm "$builddir"/Test/C02cond.ztst
	# PPC?
	rm "$builddir"/Test/V09datetime.ztst
	make test
}

package() {
	cd "$builddir"

	make DESTDIR="$pkgdir" install
	install -Dm644 "$srcdir"/zprofile "$pkgdir"/etc/zsh/zprofile
}

doc() {
	default_doc
	_submv $_sharedir/help
}

calendar() {
	pkgdesc="Calendar Function System for ZSH"
	depends="$pkgname"

	_submv $_sharedir/functions/Calendar
}

vcs() {
	pkgdesc="Version Control Information module for ZSH (vcs_info)"
	depends="$pkgname"

	_submv $_sharedir/functions/VCS_Info
}

zftp() {
	pkgdesc="Zftp Function System for ZSH"
	depends="$pkgname"

	_submv $_libdir/zsh/zftp.so
	_submv $_sharedir/functions/Zftp
}

_completion() {
	local name="${subpkgname%-zsh-completion}"
	pkgdesc="Zsh completions for $name"
	depends="$pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel $name"

	local files="$(printf '%s\n' $_comps \
		| sed -En "s|^$name:(.*)|\1|p" | tr : ' ')"
	test -n "$files" || { echo "$name not found in \$_comps" >&2; return 1; }

	local f; for f in $files; do
		_submv $_sharedir/functions/Completion/$f
	done
}

_submv() {
	local path="$1"
	mkdir -p "$subpkgdir"/${path%/*}
	mv "$pkgdir"/$path "$subpkgdir"/${path%/*}/
}

sha512sums="56a83b4a8ae97656ea5a2d7b15632a288017552c276ab306be6d242d83dc92ce871330aeb46a0e8a49e9f30f6bff32e3c956c62e5ce19361e52ea0799cb59f50  zsh-5.4.1.tar.gz
cbc184f0c4af1441eaad2a3f7255be9941c64bd5a05977aea5ca3a0a11a3d121c34c1fc9e633465eda087cd196dabf9591082117bbb42cc6e6bc9352ec2c207d  zprofile"
